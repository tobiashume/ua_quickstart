(function($, Drupal) {
  Drupal.behaviors.uaqs_fields_media = {
    attach:function() {

    // @see https://developers.google.com/youtube/iframe_api_reference

      // defaults
      var defaults = {
        ratio: 16/9,
        videoId: '',
        mute: true,
        repeat: true,
        width: $(window).width(),
        playButtonClass: 'uaqs-video-play',
        pauseButtonClass: 'uaqs-video-pause',
        muteButtonClass: 'uaqs-video-mute',
        volumeUpClass: 'uaqs-video-volume-up',
        volumeDownClass: 'uaqs-video-volume-down',
        increaseVolumeBy: 10,
        start: 0,
        minimumSupportedWidth: 600
      };

      // methods
      var bgVideo = function(node, options) {
        var options = $.extend({}, defaults, options),
        $bg_container = $('#' + options.videoId + '-bg-video-container')
        $node = $(node);

        // build container
        var videoContainer = '<div id="' + options.videoId + '-container" style="overflow: hidden; width: 100%; height: 100%" class="uaqs-video-container"><div id="' + options.videoId + '-player" style="position: absolute"></div></div>';

        $bg_container.prepend(videoContainer);
        $node.css({position: 'relative'});

        // set up iframe player, use global scope so YT api can talk
        window.player;
        window.onYouTubeIframeAPIReady = function() {
          player = new YT.Player(options.videoId + '-player', {
            width: options.width,
            height: Math.ceil(options.width / options.ratio),
            videoId: options.videoId,
            playerVars: {
              modestbranding: 1,
              controls: 0,
              showinfo: 0,
              rel: 0,
              wmode: 'transparent'
            },
            events: {
              'onReady': onPlayerReady,
              'onStateChange': onPlayerStateChange
            }
          });
        }

        window.onPlayerReady = function(e) {
          if (options.mute) e.target.mute();
          e.target.seekTo(options.start);
          e.target.playVideo();
        }

        window.onPlayerStateChange = function(state) {
          if (state.data === 1) {
            $('#' + options.videoId + '-bg-video-container').addClass('uaqs-video-playing').removeClass( 'uaqs-video-loading');
          }
          if (state.data === 0 && options.repeat) { // video ended and repeat option is set true
              player.seekTo(options.start); // restart
          }
        }

        // resize handler updates width, height and offset of player after resize/init
        var resize = function() {
          var width = $($bg_container).width(),
            pWidth, // player width, to be defined
            height = $($bg_container).height(),
            pHeight, // player height, tbd
            $bgVideoPlayer = $('#' + options.videoId + '-player');

          // when screen aspect ratio differs from video, video must center and underlay one dimension
          if (width / options.ratio < height) { // if new video height < window height (gap underneath)
            pWidth = Math.ceil(height * options.ratio); // get new player width
            $bgVideoPlayer.width(pWidth).height(height).css({left: (width - pWidth) / 2, top: 0}); // player width is greater, offset left; reset top
          } else { // new video width < window width (gap to right)
            pHeight = Math.ceil(width / options.ratio); // get new player height
            $bgVideoPlayer.width(width).height(pHeight).css({left: 0, top: (height - pHeight) / 2}); // player height is greater, offset top; reset left
          }
        }

        // events
        $(window).on('resize.bgVideo', function() {
          resize();
        })
        $(window).bind('load', function() {
          resize();
        });

        $('#' + options.videoId + '-bg-video-container').on('click','.' + options.playButtonClass, function(e) { // play button
            e.preventDefault();
            player.playVideo();
            $('#' + options.videoId + '-bg-video-container').removeClass('uaqs-video-paused').addClass('uaqs-video-playing' );
        }).on('click', '.' + options.pauseButtonClass, function(e) { // pause button
            e.preventDefault();
            player.pauseVideo();
            $('#' + options.videoId + '-bg-video-container').removeClass('uaqs-video-playing').addClass('uaqs-video-paused');
        }).on('click', '.' + options.muteButtonClass, function(e) { // mute button
            e.preventDefault();
            (player.isMuted()) ? player.unMute() : player.mute();
        }).on('click', '.' + options.volumeDownClass, function(e) { // volume down button
            e.preventDefault();
            var currentVolume = player.getVolume();
            if (currentVolume < options.increaseVolumeBy) currentVolume = options.increaseVolumeBy;
            player.setVolume(currentVolume - options.increaseVolumeBy);
        }).on('click', '.' + options.volumeUpClass, function(e) { // volume up button
            e.preventDefault();
            if (player.isMuted()) player.unMute(); // if mute is on, unmute
            var currentVolume = player.getVolume();
            if (currentVolume > 100 - options.increaseVolumeBy) currentVolume = 100 - options.increaseVolumeBy;
            player.setVolume(currentVolume + options.increaseVolumeBy);
        })
      }

        // load yt iframe js api
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        // create plugin
        $.fn.bgVideo = function (options) {
          return this.each(function () {
            if (!$.data(this, 'bgVideo_instantiated')) { // let's only run one
              $.data(this, 'bgVideo_instantiated',
              bgVideo(this, options));
            }
          });
        }
      }
    }
}(jQuery, Drupal));

