7.x-1.0-alpha33, 2019-03-07
-----------------------------

7.x-1.0-alpha33, 2019-02-20
-----------------------------
- UADIGITAL-1914: Bump the link contrib module version because of a security issue.

7.x-1.0-alpha32, 2019-02-11
-----------------------------

7.x-1.0-alpha30, 2019-02-01
-----------------------------

7.x-1.0-alpha29, 2019-01-28
-----------------------------

7.x-1.0-alpha28, 2019-01-17
-----------------------------

7.x-1.0-alpha27, 2019-01-16
-----------------------------

7.x-1.0-alpha26, 2019-01-11
-----------------------------

7.x-1.0-alpha25, 2018-11-15
-----------------------------

7.x-1.0-alpha24, 2018-11-02
-----------------------------

7.x-1.0-alpha23, 2018-10-17
-----------------------------

7.x-1.0-alpha22, 2018-10-11
-----------------------------

7.x-1.0-alpha21, 2018-10-10
-----------------------------

7.x-1.0-alpha20, 2018-09-21
-----------------------------

7.x-1.0-alpha19, 2018-08-29
-----------------------------
- Preparing to tag 7.x-1.0-alpha17 for uaqs_alphabetical_list.
- Preparing to tag 7.x-1.0-alpha16 for uaqs_alphabetical_list.
- UADIGITAL-1662 Backport arizona.edu updates to alphabetical list view.
- Preparing to tag 7.x-1.0-alpha15 for uaqs_alphabetical_list.
- Preparing to tag 7.x-1.0-alpha14 for uaqs_alphabetical_list.
- Preparing to tag 7.x-1.0-alpha13 for uaqs_alphabetical_list.
- Preparing to tag 7.x-1.0-alpha12 for uaqs_alphabetical_list.
- Preparing to tag 7.x-1.0-alpha11 for uaqs_alphabetical_list.
- Preparing to tag 7.x-1.0-alpha10 for uaqs_alphabetical_list.
- UADIGITAL-1453 make search field label customizable using a second argument.
- UADIGITAL-1443 clean up demo content for php7
- UADIGITAL-1420 Fix UAQS alphabetical list in-page search speed and allow hierarchal search.
- Copy in the code for the new module.
- Add the initial files.
7.x-1.0-alpha17, 2018-07-30
-----------------------------

7.x-1.0-alpha16, 2018-07-20
-----------------------------
- UADIGITAL-1662 Backport arizona.edu updates to alphabetical list view.

7.x-1.0-alpha15, 2018-06-19
-----------------------------

7.x-1.0-alpha14, 2018-05-18
-----------------------------

7.x-1.0-alpha13, 2018-04-25
-----------------------------

7.x-1.0-alpha12, 2018-04-20
-----------------------------

7.x-1.0-alpha11, 2018-03-30
-----------------------------

7.x-1.0-alpha10, 2018-03-09
-----------------------------
- UADIGITAL-1453 make search field label customizable using a second argument.
- UADIGITAL-1443 clean up demo content for php7
- UADIGITAL-1420 Fix UAQS alphabetical list in-page search speed and allow hierarchal search.
- Copy in the code for the new module.
- Add the initial files.
