# Makefile for the UA Quickstart Distribution
# ===========================================

core: 7.x
api: '2'
projects:

  # UA Contrib Modules
  # ------------------
  ua_cas:
    subdir: custom
    download:
      branch: 7.x-1.x
      revision: '162f05a'
      type: git
      url: 'https://bitbucket.org/ua_drupal/ua_cas.git'
    type: module

  ua_google_tag:
    subdir: custom
    download:
      branch: 7.x-1.x
      revision: '3f46e02'
      type: git
      url: 'https://bitbucket.org/ua_drupal/ua_google_tag.git'
    type: module

  # Contrib modules
  # ---------------
  access_unpublished:
    patch:
      # Add timestamp to hashed url to bypass edge-cache
      # @see https://www.drupal.org/node/2957086
      2957086: 'https://www.drupal.org/files/issues/2018-05-09/2957086-6-add-cachebusting-timestamp-to-hash-url.patch'
      # Error when adding a file_entity within a paragraphs_item in new content/node when using workbench moderation.
      # @see https://www.drupal.org/node/2968178
      2968178: 'https://www.drupal.org/files/issues/2018-04-30/2968178-error-when-adding-file-entity-within-a-paragraph-item-in-new-content.patch'
    version: '1.03'
  admin_views:
    version: '1.6'
  advanced_help:
    version: '1.5'
  auto_entitylabel:
    version: '1.4'
  bean:
    patch:
      # Fix duplicate title/header issue.
      # @see https://jira.arizona.edu/browse/UADIGITAL-1765
      # @see https://www.drupal.org/node/2996282
      2996282: 'https://www.drupal.org/files/issues/2018-09-28/bean-duplicate_headers-2996282-3.patch'
    version: '1.13'
  breakpoints:
    version: '1.6'
  coffee:
    version: '2.3'
  ctools:
    version: '1.14'
  d8cache:
    version: '1.2'
  date:
    version: '2.11-beta2'
  date_ap_style:
    version: '1.3'
  date_ical:
    version: '3.9'
  defaultconfig:
    version: '1.0-alpha11'
  dialog:
    version: '2.0-beta1'
  diff:
    version: '3.4'
  draggableviews:
    version: '2.1'
  drafty:
    version: '1.0-rc1'
  editor:
    patch:
      # Add hook_editor_ckeditor_settings_alter to API documentation
      # @see https://jira.arizona.edu/browse/UADIGITAL-809
      # @see https://www.drupal.org/node/2756601
      2756601-3: 'https://www.drupal.org/files/issues/editor-ckeditor_settings_api_doc-2756601-3.patch'
      # Fix issue causing Editor to be unloaded on AJAX submissions.
      # @see https://jira.arizona.edu/browse/UADIGITAL-1259
      # @see https://www.drupal.org/node/2691101
      2691101: 'https://www.drupal.org/files/issues/editor-ckeditor-unloads-on-ajax-2691101-2-7.41.patch'
    version: '1.0-alpha7'
  elements:
    version: '1.5'
  email:
    version: '1.3'
  entity:
    version: '1.9'
  entityreference:
    patch:
      # Fixes Does not work with version 7.x-3.x of entity_embed
      # @see https://jira.arizona.edu/browse/UADIGITAL-1229
      # @see https://www.drupal.org/node/2903245
      2903245-5: 'https://www.drupal.org/files/issues/handle-entity-embed-2903245-5.patch'
    version: '1.5'
  entity_embed:
    patch:
      # Prevent Drush from applying dev version updates.
      # This patch should be removed once a release version can be used.
      lock: 'https://bitbucket.org/!api/2.0/snippets/ua_drupal/xjE7e5/f0d2d13/files/drush-lock-update.patch'
    download:
      branch: 7.x-3.x
      revision: bb4f5e3
      type: git
      url: 'https://git.drupal.org/project/entity_embed.git'
    type: module
  exclude_node_title:
    version: '1.9'
  extlink:
    patch:
      # Fixes external link detection for site URLs containing port numbers.
      # @see https://jira.arizona.edu/browse/UADIGITAL-1754
      # @see https://www.drupal.org/project/extlink/issues/3004486
      3004486: 'https://www.drupal.org/files/issues/2018-10-04/urls_containing_port-3004486-7.patch'
    version: '1.20'
  features:
    patch:
      986968: 'https://www.drupal.org/files/issues/986968-shortcut-sets_0.patch'
    version: '2.11'
  feeds:
    version: '2.0-beta4'
  feeds_tamper:
    version: '1.2'
  feeds_jsonpath_parser:
    version: '1.0'
  fences:
    version: '1.2'
  field_collection:
    patch:
      # Fixes error EntityFieldQueryException: Unknown field
      # @see https://jira.arizona.edu/browse/UADIGITAL-1801
      # @see https://www.drupal.org/project/field_collection/issues/3029628
      3029628: 'https://www.drupal.org/files/issues/2019-01-30/re_apply_fix_from-3029628-2.patch'
    version: '1.0-beta13'
  field_formatter_settings:
    version: '1.1'
  field_group:
    version: '1.6'
  field_group_background_image:
    patch:
      # Adds picture module support
      2502001 : 'https://www.drupal.org/files/issues/2018-08-14/field_group_background_image-convert-multiplier-to-integer-2502001-18-D7.patch'
    version: '1.1'
  field_group_link:
    version: '1.5'
  field_multiple_limit:
    patch:
      # Fixes PHP Notice for multi-valued fields hidden in Field UI.
      # @see https://jira.arizona.edu/browse/UADIGITAL-924
      # @see https://www.drupal.org/node/2807079
      2807079: 'https://www.drupal.org/files/issues/Avoid_php_warnings-2807079-2.patch'
    version: '1.0-alpha5'
  file_entity:
    patch:
      # Fixes screen reader issue with images linking to file entity when it should not.
      # @see https://www.drupal.org/project/file_entity/issues/1245266.
      1245266: 'https://www.drupal.org/files/issues/2018-03-18/file_entity-rendered_file_template-1245266-17.patch'
    version: '2.25'
  file_entity_inline:
    version: '1.0-beta1'
  flag:
    version: '3.9'
  flexslider:
    version: '2.0-rc2'
  google_analytics:
    version: '2.5'
  globalredirect:
    version: '1.6'
  honeypot:
    version: '1.25'
  html5_tools:
    version: '1.3'
  image_class:
    patch:
      # Prevent Drush from applying dev version updates.
      # This patch should be removed once a release version can be used.
      lock: 'https://bitbucket.org/!api/2.0/snippets/ua_drupal/xjE7e5/f0d2d13/files/drush-lock-update.patch'
    download:
      branch: 7.x-1.x
      revision: a4baf33
      type: git
    version: 1.x-dev
  image_link_formatter:
    version: '1.1'
  job_scheduler:
    version: 2.0
  jquery_update:
    version: '2.7'
  libraries:
    version: '2.5'
  link:
    version: '1.6'
  manualcrop:
    version: '1.7'
  masquerade:
    version: '1.0-rc7'
  match_redirect:
    version: '1.0'
  media:
    version: '2.21'
  media_oembed:
    version: '2.7'
  menu_bean:
    download:
      # Patch fails unless applied to version of .info file without d.o. modifications.
      tag: 7.x-1.0-beta2
      type: git
    patch:
      2714007: 'https://drupal.org/files/issues/bean_plugin_class_not-2714007-2_0.patch'
      # Fixes PHP Notice when creating new Menu Bean.
      # @see https://jira.arizona.edu/browse/UADIGITAL-1817
      # @see https://www.drupal.org/project/menu_bean/issues/2617368
      2617368: 'https://www.drupal.org/files/issues/menu_bean-undefined_index_parent_mlid-2617368_0.patch'
    version: 1.0-beta2
  menu_block:
    version: '2.7'
  menu_position:
    version: '1.2'
  metatag:
    version: '1.25'
  migrate:
    patch:
      # PHP 7.0 compatibility.
      # @see https://jira.arizona.edu/browse/UADIGITAL-1484
      # @see https://www.drupal.org/node/2928784
      # @see https://www.drupal.org/node/2996693
      2928784: 'https://www.drupal.org/files/issues/2018-03-12/mssql-functions-2928784-11.patch'
      2996693: 'https://www.drupal.org/files/issues/2018-09-01/fix_warnings_sqlsrv.inc-2996693-2.patch'
    version: '2.11'
  module_filter:
    version: '2.1'
  multiform:
    version: '1.6'
  navbar:
    version: '1.7'
  paragraphs:
    patch:
      # PHP 7.2 compatibility.
      # @see https://jira.arizona.edu/browse/UADIGITAL-1484
      # @see https://www.drupal.org/node/2951390
      # @see https://jira.arizona.edu/browse/UADIGITAL-1873
      # @see https://www.drupal.org/project/paragraphs/issues/3010938
      2951390: 'https://www.drupal.org/files/issues/2018-03-08/deprecated_each-2951390-2.patch'
      3010938: 'https://www.drupal.org/files/issues/2018-11-26/paragraphs-count-php71-3010938-3.patch'
      # Fixes access control on paragraphs revisions.
      # @see https://www.drupal.org/project/paragraphs/issues/2562875
      2562875: 'https://www.drupal.org/files/issues/paragraphs-fix-access-host-entity-properties-not-populated-PT-149611197-custom.patch'
      # Fixes access control on paragraphs revisions.
      # @see https://www.drupal.org/project/paragraphs/issues/2594593
      2594593: 'https://www.drupal.org/files/issues/paragraphs_revision_access-2594593-21.patch'
    version: '1.0-rc5'
  pathauto:
    version: '1.3'
  picture:
    version: '2.13'
  plupload:
    version: '1.7'
  redirect:
    version: '1.0-rc3'
  regionclass:
    version: '1.0-rc2'
  special_menu_items:
    patch:
      # Fixes issue causing preprocess functions to get called twice.
      # @see https://jira.arizona.edu/browse/UAMS-547
      # @see https://www.drupal.org/node/1447864
      1447864: 'https://www.drupal.org/files/special_menu_items-call_theme_once-1447864-6.patch'
      # Nolink special menu items are skipped on keyboard navigation.
      # @see https://jira.arizona.edu/browse/UADIGITAL-1188
      # @see https://www.drupal.org/node/2291085
      2291085: 'https://www.drupal.org/files/issues/nolink_tabindex-2291085-1.patch'
    version: '2.0'
  simplified_menu_admin:
    version: '1.0'
  strongarm:
    version: '2.0'
  superfish:
    version: '2.0'
  taxonomy_menu:
    version: '1.6'
  telephone:
    version: '1.0-alpha1'
  title:
    version: '1.0-alpha9'
  token:
    version: '1.7'
  transliteration:
    version: '3.2'
  viewfield:
    version: '2.1'
  views:
    patch:
      # Fixes PHP 7.1 warnings generated by views_plugin_pager_full->query().
      # @see https://jira.arizona.edu/browse/UADIGITAL-1870
      # @see https://www.drupal.org/project/views/issues/2885660
      2885660: 'https://www.drupal.org/files/issues/2018-06-28/2885660-13.patch'
    version: '3.20'
  views_bootstrap:
    version: '3.2'
  views_bulk_operations:
    version: '3.5'
  views_content_cache:
    version: '3.0-alpha3'
  views_field_view:
    version: '1.2'
  webform:
    version: '4.19'
  workbench_moderation:
    version: '3.0'
  xautoload:
    version: '5.7'
  xmlsitemap:
    version: '2.6'

  # UA Contrib Themes
  # -----------------
  adminimal_theme:
    subdir: ''
    type: theme
    version: '1.25'

# Set the default subdirectory to hold contrib modules
# ----------------------------------------------------
defaults:
  projects:
    subdir: contrib

# Libraries
# ---------
libraries:
  autogrow:
    download:
      type: get
      url: 'https://download.ckeditor.com/autogrow/releases/autogrow_4.6.0.zip'

  backbone:
    download:
      tag: 1.0.0
      type: git
      url: https://github.com/jashkenas/backbone.git

  btgrid:
    download:
      type: get
      url: 'https://download.ckeditor.com/btgrid/releases/btgrid_1.0b1.zip'

  bt_table:
    download:
      type: get
      url: 'https://download.ckeditor.com/bt_table/releases/bt_table_1.0b2.zip'

  flexslider:
    directory_name: flexslider
    download:
      type: get
      url: 'https://github.com/woothemes/FlexSlider/archive/version/2.4.0.tar.gz'

  iCalcreator:
    download:
      type: get
      url: 'https://github.com/iCalcreator/iCalcreator/archive/e3dbec2cb3bb91a8bde989e467567ae8831a4026.zip'
    patch:
      # PHP7 Compatibility patch
      # @see https://jira.arizona.edu/browse/UADIGITAL-1437
      # @see https://www.drupal.org/node/2707373
      2707373: 'https://www.drupal.org/files/issues/iCalcreator-php-7-2707373-6.patch'

  jsonpath:
    download:
      type: get
      url: 'https://cdn.uadigital.arizona.edu/lib/jsonpath/0.8.3/src/php/jsonpath.php'

  jquery.imagesloaded:
    download:
      type: get
      url: 'https://github.com/desandro/imagesloaded/archive/v2.1.2.tar.gz'

  jquery.imgareaselect:
    download:
      type: get
      url: 'https://github.com/odyniec/imgareaselect/archive/v0.9.11-rc.1.tar.gz'

  modernizr:
    download:
      revision: 5b89d92
      type: git
      url: https://github.com/BrianGilbert/modernizer-navbar.git

  underscore:
    download:
      tag: 1.5.0
      type: git
      url: https://github.com/jashkenas/underscore.git
